
create database if not exists desafio default character set utf8 collate utf8_general_ci;
use desafio;

create table if not exists categoria(
idcategoria int auto_increment not null primary key,
descricao varchar(50) not null)
default character set utf8 default collate utf8_general_ci;

create table if not exists produtos(
sku int auto_increment not null primary key,
nome varchar(80) not null,
preco float not null,
quantidade int not null,
detalhes varchar(100),
idcategoria int not null,
constraint fk_categoria foreign key(idcategoria) references categoria(idcategoria))
default character set utf8 default collate utf8_general_ci;



